## evergreen-user 11 RP1A.200720.011 V12.5.2.0.RGBMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6833
- Codename: evergreen
- Brand: POCO
- Flavor: evergreen-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.2.0.RGBMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/evergreen_global/evergreen:11/RP1A.200720.011/V12.5.2.0.RGBMIXM:user/release-keys
- OTA version: 
- Branch: evergreen-user-11-RP1A.200720.011-V12.5.2.0.RGBMIXM-release-keys
- Repo: poco_evergreen_dump_10044


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
